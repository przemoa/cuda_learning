/*
 *
 * To optimize:
 * - memory can be fetched as 2D texture
 * - each force is calculated twice
 * - group data for faster memory fetching (position, velocity, mass, ... for every planet in continuous memory)
 */

#include "universe.h"


#include <helper_functions.h>    // includes cuda.h and cuda_runtime_api.h
#include <helper_cuda.h>         // helper functions for CUDA error check

#include "math.h"
#include <assert.h>





__global__ void simulateUniverse_updateVelocity(TUniverseData universe)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= NUMBER_OF_PLANETS)
		return;

	if (!universe.alive[index])
		return;

	for (int i = 0; i < NUMBER_OF_PLANETS; i++)
	{
		if (!universe.alive[i])
			continue;

		if (i == index)
			continue;

		float forceDirection[3];
		float distanceSquared = 0;
		for (int j = 0; j < 3; j++)
		{
			const float distance1d = universe.position[i].val[j] - universe.position[index].val[j];
			forceDirection[j] = distance1d;
			distanceSquared += distance1d * distance1d;
		}

		const float distance = sqrtf(distanceSquared);

		if (distance < 0.00001f)
		{
			assert(0);  // handle to close distance - merge planets!
			continue;
		}

		const float force = GRAVITY_CONSTANT * universe.mass[index] * universe.mass[i] / distanceSquared;
		const float k = force * distance;

		for (int j = 0; j < 3; j++)
		{
			const float forceComponent = k * forceDirection[j];
			universe.velocity[index].val[j] += forceComponent / universe.mass[index] * universe.simulationStep;
		}
	}
}

__global__ void simulateUniverse_updatePosition(TUniverseData universe)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= NUMBER_OF_PLANETS)
		return;

	if (!universe.alive[index])
		return;

	for (int j = 0; j < 3; j++)
	{
		const float d = universe.velocity[index].val[j] * universe.simulationStep;
		universe.position[index].val[j] += d;
	}

	universe.tmp[index] = universe.position[index].val[1];
}

__global__ void initializeUniverse(TUniverseData universe)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= NUMBER_OF_PLANETS)
		return;


//	universe.position[index].val[POS_X] = 10*cosf(2*3.14*index/NUMBER_OF_PLANETS-2) - index % 7 + 3;
//	universe.position[index].val[POS_Y] = 10*sinf(2*3.14*index/NUMBER_OF_PLANETS);
//	universe.position[index].val[POS_Z] = index % 10 - 5;
//	universe.position[index].val[3] = 1;  // 1=point

	universe.velocity[index].val[POS_X] = 1*sinf(2*3.14*index/NUMBER_OF_PLANETS);
	universe.velocity[index].val[POS_Y] = 0.22*cosf(2*3.14*index/NUMBER_OF_PLANETS-2);
	universe.velocity[index].val[POS_Z] = 0;

	universe.mass[index] = 1;
	universe.alive[index] = true;

	universe.color[index].vec = make_float3(1, 0, 0);

	for (int i = 0; i < 3; i++)
	{
		universe.position[index].val[i] *= UNIVERSE_SCALE;
		universe.velocity[index].val[i] *= UNIVERSE_SCALE;
	}


	universe.tmp[index] = 0;
}

void setupUniverse(TUniverseData* pUniverse)
{
	checkCudaErrors( cudaMallocManaged((void**)&(pUniverse->velocity), sizeof(pUniverse->velocity[0]) * NUMBER_OF_PLANETS) );
	checkCudaErrors( cudaMalloc((void**)&(pUniverse->mass), sizeof(pUniverse->mass[0]) * NUMBER_OF_PLANETS) );
	checkCudaErrors( cudaMalloc((void**)&(pUniverse->color), sizeof(pUniverse->color[0]) * NUMBER_OF_PLANETS) );
	checkCudaErrors( cudaMalloc((void**)&(pUniverse->alive), sizeof(pUniverse->alive[0]) * NUMBER_OF_PLANETS) );

	checkCudaErrors( cudaMallocManaged((void**)&(pUniverse->tmp), sizeof(pUniverse->tmp[0]) * NUMBER_OF_PLANETS) );

	float4* positionsBuffer = new float4[NUMBER_OF_PLANETS];
	TPoint4* positions = (TPoint4*) positionsBuffer;
	for (int i = 0; i < NUMBER_OF_PLANETS; i++)
	{
		positions[i].val[POS_X] = 10*cosf(2*3.14*i/NUMBER_OF_PLANETS-2) - (rand() % 700 - 350) / 100.0;
		positions[i].val[POS_Y] = 10*sinf(2*3.14*i/NUMBER_OF_PLANETS) - (rand() % 70 - 35) / 100.0;
		positions[i].val[POS_Z] = (rand() % 1000 - 500) / 100.0;
		positions[i].val[3] = 1;  // 1=point
	}
	checkCudaErrors(
			cudaMemcpy(
					(void**)(pUniverse->position),
					positions,
					sizeof(pUniverse->position[0]) * NUMBER_OF_PLANETS,
					cudaMemcpyHostToDevice)
					);
	delete[] positionsBuffer;

	initializeUniverse<<< THREADS_PER_BLOCK, BLOCKS_COUNT >>>(*pUniverse);

}

void simulateUniverse(TUniverseData* pUniverse)
{
	simulateUniverse_updateVelocity<<< THREADS_PER_BLOCK, BLOCKS_COUNT >>>(*pUniverse);
	simulateUniverse_updatePosition<<< THREADS_PER_BLOCK, BLOCKS_COUNT >>>(*pUniverse);

//	checkCudaErrors( cudaDeviceSynchronize() );  // required before printing ...
//	std::cout << "V0 ("
//			<< pUniverse->velocity[0].val[0]  << ", "
//			<< pUniverse->velocity[0].val[1]  << ", "
//			<< pUniverse->velocity[0].val[2]  << ")     "
//			<< pUniverse->tmp[0] << "   "
//			<< std::endl;
}

