
#define NUMBER_OF_PLANETS 1024*2
#define GRAVITY_CONSTANT 0.0003f

#define POS_X 0
#define POS_Y 1
#define POS_Z 2

#define THREADS_PER_BLOCK 64
#define BLOCKS_COUNT ((NUMBER_OF_PLANETS + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK)
#define UNIVERSE_SCALE 3

union TPoint4
{
	float4 vec;
	float val[4];
};

union TPoint3
{
	float3 vec;
	float val[3];
};


struct TUniverseData
{
	TPoint4* position = nullptr;  // xyz1
	TPoint3* velocity = nullptr;  // Vx Vy Vz

	float* mass = nullptr;
	TPoint3* color = nullptr;  // color for each planet
	bool* alive = nullptr;  // if planet is still alive (not merged with another planet and not too far from the visible universe

	float simulationStep = 0.1f;

	float* tmp = nullptr;
};


void setupUniverse(TUniverseData* pUniverse);
void simulateUniverse(TUniverseData* pUniverse);
