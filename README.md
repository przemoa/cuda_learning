# cuda_learning

1. Setup
- uninstall EVERYTHING nvidia related
```
sudo apt purge nvidia*
sudo apt purge cuda*
sudo apt autoremove
```
- install newest nvidia-driversXXX
- download and install cuda toolkit, but uncheck 'drivers' during installation
- install additional libraries
```
sudo apt-get install g++ freeglut3-dev build-essential libx11-dev \
    libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev
```
- confirm that nvcc is visible in path
- install nvtop (htop fot gpu)
```
git clone https://github.com/Syllo/nvtop
cd nvtop/
mkdir build
cd build/
cmake ..
sudo apt-get install libncurses5-dev
cmake ..
make
sudo make install
nvtop 
```
